### Maya's personal site

This exists to host essays and interesting projects I make in a publicly-available and accessible format. 

The directory structure should be relatively self-explanatory; HTML and CSS components are stored in their respective folders. The src/ folder holds Rust source code for the wrapper around Tera, the template language I use. Run `cargo run` to compile the program and render the finished HTML documents. 