/* A simple wrapper around Tera (https://github.com/Keats/tera)
that renders template language */

extern crate tera;

use tera::{Tera, Context};
use std::fs::{File, read_dir};
use std::path::Path;
use std::io::Write;

fn main() {
    let te = Tera::new("html/**/*").unwrap();

    let path = Path::new("html/");
    let files = read_dir(&path).unwrap();
    let mut file;
    for file_res in files {
        if let Ok(f) = file_res {
            file = f;
        } else { continue; }
        let name = file.file_name().into_string().unwrap();
        if file.file_type().unwrap().is_file() {
            let rendered_file_text = te.render(&name, &Context::new()).unwrap();
            let mut out_file = File::create(format!("public/{}", &name)).unwrap();
            out_file.write_all(&rendered_file_text.into_bytes()).unwrap();
            println!("Rendered file {}", name);
        }
    }

    println!("Done.");
}
